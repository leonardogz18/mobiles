package com.te.broadcasttutorial;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by leonard on 02/11/15.
 */
public class CustomReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent){
        Toast.makeText(context, "Custom Broadcast Received", Toast.LENGTH_LONG).show();
    }
}
