package com.te.broadcasttutorial;

/**
 * Created by leonard on 31/10/15.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class AirplaneReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent){
        Toast.makeText(context,"Airplane Mode Changed",Toast.LENGTH_SHORT).show();
    }
}
