package com.te.broadcasttutorial;

/**
 * Created by leonard on 02/11/15.
 */
import android.app.Activity;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends Activity {

    static final Uri CONTENT_URL = Uri.parse(
            "content://com.te.broadcasttutorial.ContactProvider/cpcontacts");

    TextView contactsTextView = null;

    ContentResolver resolver;

    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.second_activity);
        textView = (TextView) findViewById(R.id.textView2);

        super.onCreate(savedInstanceState);

        resolver = getContentResolver();
        contactsTextView = (TextView) findViewById(R.id.contactsTextView);

        getContacts();
    }

    public void getContacts(){
        String[] projection = new String[]{"id","name"};

        Cursor cursor = resolver.query(CONTENT_URL, projection, null, null, null);
        String contactList = "";

        if(cursor.moveToFirst()){

            do{
                String id = cursor.getString(cursor.getColumnIndex("id"));

                String name = cursor.getString(cursor.getColumnIndex("name"));

                contactList = contactList + id + " : " + name + "\n";
            }while(cursor.moveToNext());

        }

        contactsTextView.setText(contactList);
    }
}
