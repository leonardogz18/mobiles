package com.te.broadcasttutorial;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

/**
 * Created by leonard on 06/11/15.
 */
public class CommentsDataProvider extends ContentProvider {

    private static final String AUTH = "com.te.broadcasttutorial.CommentsDataProvider";
    public static final Uri COMMENTS_URI = Uri.parse("content://" + AUTH + "/" + DbHelper.TABLE_NAME);

    final static int COMMENTS = 1;

    SQLiteDatabase db;
    DbHelper dbHelper;

    private final static UriMatcher uriMatcher;

    static{
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTH,DbHelper.TABLE_NAME, COMMENTS);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] strings, String s, String[] strings1, String s1) {
        Cursor cursor;
        db = dbHelper.getWritableDatabase();

        cursor = db.query(DbHelper.TABLE_NAME, strings, s, strings1,null,null,s1);

        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }
    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        db = dbHelper.getWritableDatabase();

        if(uriMatcher.match(uri) == COMMENTS){
            db.insert(DbHelper.TABLE_NAME, null, contentValues);
        }
        db.close();

        getContext().getContentResolver().notifyChange(uri,null);
        return null;
    }

    @Override
    public int delete(Uri uri, String s, String[] strings) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String s, String[] strings) {
        return 0;
    }
}
